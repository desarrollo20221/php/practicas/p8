<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <form action="ejercicio3Destino.php" method="get">
            <div>
                <label for="nombre">
                    Nombre:
                </label>
                <input type="text" name="nombre" id="nombre" placeholder="Introduce tu nombre">
            </div> 
            <div>
                <label for="email">
                    Email: 
                </label>
                <input type="email" name="email" id="email" placeholder="Introduce tu correo">
            </div>
            <div>
                <label for="telefono">
                    Telefono: 
                </label>
                <input type="tel" name="telefono" id="telefono" placeholder="Introduce tu telefono">
            </div>
            <div>
                <button>Enviar</button>
            </div>
        </form>
        <?php
        ?>
    </body>
</html>
