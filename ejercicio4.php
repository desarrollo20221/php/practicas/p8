<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <form action="ejercicio4Destino.php">
            <div>
                <label for="nombre">
                    Nombre:
                </label>
                <input type="text" name="nombre" id="nombre" placeholder="Introduce tu nombre">
            </div> 
            <div>
                <label for="email">
                    Email: 
                </label>
                <input type="email" name="email" id="email" placeholder="Introduce tu correo">
            </div> 
            <button formmethod="get">Enviar por GET</button>
            <button formmethod="post">Enviar por POST</button>
        </form>
        <?php
        // put your code here
        ?>
    </body>
</html>
