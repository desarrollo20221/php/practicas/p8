<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <form action="ejercicio5Destino.php">
            <div>
                <label for="nombre">
                    Nombre:
                </label>
                <input type="text" name="nombre" id="nombre" placeholder="Introduce tu nombre" required="">
            </div> 
            <div>
                <label for="apellido">
                    Apellidos:
                </label>
                <input type="text" name="apellido" id="apellido" placeholder="Introduce tu apellido" required="">
            </div>
            <div>
                <label for="cp">
                    Codigo Posta:
                </label>
                <input type="text" name="cp" id="cp" placeholder="Introduce tu Codigo Postal" required="">
            </div> 
            <div>
                <label for="tel">
                    Telefono: 
                </label>
                <input type="tel" name="tel" id="tel" placeholder="Introduce tu Telefono" required="">
            </div>
            <div>
                <label for="correo">
                    Correo: 
                </label>
                <input type="email" name="correo" id="correo" placeholder="Introduce tu correo" required="">
            </div>
            <button formmethod="get">Enviar</button>
           
        </form>
    </body>
</html>
